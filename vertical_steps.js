Drupal.behaviors.vertical_steps = function (context) {
  $(context).find("#edit-next:not(.vertical_steps-processed)").click(function(e) {
    e.preventDefault();
    $(context).find(".vertical-tabs-panes fieldset:visible").hide().next('fieldset').show();
    $(context).find(".vertical-tabs-list li.selected").removeClass('selected').next('li').addClass('selected');
    $('html,body').animate({scrollTop: (0)}, 500);
    if (!$(context).find(".vertical-tabs-panes fieldset:visible").next('fieldset').length) {
      $(context).find("#edit-next").hide();
    }
    if ($(context).find(".vertical-tabs-panes fieldset:visible").prev('fieldset').length) {
      $(context).find("#edit-prev").show();
    }
    return false;
  }).addClass('vertical_steps-processed');
  $(context).find("#edit-prev:not(.vertical_steps-processed)").click(function(e) {
    e.preventDefault();
    $(context).find(".vertical-tabs-panes fieldset:visible").hide().prev('fieldset').show();
    $(context).find(".vertical-tabs-list li.selected").removeClass('selected').prev('li').addClass('selected');
    $('html,body').animate({scrollTop: (0)}, 500);
    if (!$(context).find(".vertical-tabs-panes fieldset:visible").prev('fieldset').length) {
      $(context).find("#edit-prev").hide();
    }
    if ($(context).find(".vertical-tabs-panes fieldset:visible").next('fieldset').length) {
      $(context).find("#edit-next").show();
    }
    return false;
  }).addClass('vertical_steps-processed');

  // The li.vertical-tab-button's are added by the vertical_tabs js.  We can't
  // guarantee that it's run already, so let's set this aside for a few
  // milliseconds.  Because javascript is single-threaded, we know this won't
  // run until it's done processing all the behaviors.
  setTimeout(function () {
    $(context).find(".vertical-tab-button a:not(.vertical_steps-processed)").click(function() {
      if ($(context).find(".vertical-tabs-panes fieldset:visible").prev('fieldset').length) {
        $(context).find("#edit-prev").show();
      }
      else {
        $(context).find("#edit-prev").hide();
      }
      if ($(context).find(".vertical-tabs-panes fieldset:visible").next('fieldset').length) {
        $(context).find("#edit-next").show();
      }
      else {
        $(context).find("#edit-next").hide();
      }
    }).addClass('vertical_steps-processed');
  }, 100);
}
