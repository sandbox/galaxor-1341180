Vertical Steps
--------------

This module places "Prev" and "Next" buttons on the bottoms of forms that have
vertical-tabs.  Clicking these buttons will switch you between the
vertical-tabs and scroll you back to the top.

Without this module, users see a bunch of vertical tabs.  One is highlighted.
When they scroll to the bottom, they see a Save button and may not notice that
there are other tabs.

The presence of Prev and Next buttons alerts people that there is more to the
form, and lets them switch to the next tab without scrolling back up.
